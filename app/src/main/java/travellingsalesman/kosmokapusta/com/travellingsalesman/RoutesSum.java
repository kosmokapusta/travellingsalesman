package travellingsalesman.kosmokapusta.com.travellingsalesman;

/**
 * Created by kosmokapusta on 08.01.15.
 */
public class RoutesSum {
    // Holds sum of routes
    Route[] routes;

    // Construct a sum
    public RoutesSum (int size, boolean initialise) {
        routes = new Route[size];
        // If we need to initialise a sum of routes do so
        if (initialise) {
            // Loop and create individuals
            for (int i = 0; i < getSize(); i++) {
                Route newTour = new Route();
                newTour.generateIndividual();
                saveTour(i, newTour);
            }
        }
    }

    public void saveTour(int index, Route tour) {
        routes[index] = tour;
    }

    public Route getRoute(int index) {
        return routes[index];
    }

    // Gets the best route of the sum
    public Route getFittest() {
        Route fittest = routes[0];
        // Loop through individuals to find fittest
        for (int i = 1; i < getSize(); i++) {
            if (fittest.getFitness() <= getRoute(i).getFitness()) {
                fittest = getRoute(i);
            }
        }
        return fittest;
    }

    // Get size
    public int getSize() {
        return routes.length;
    }
}

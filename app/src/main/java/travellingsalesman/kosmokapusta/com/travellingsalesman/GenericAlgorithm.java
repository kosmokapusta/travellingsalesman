package travellingsalesman.kosmokapusta.com.travellingsalesman;

/**
 * Created by kosmokapusta on 08.01.15.
 */
public class GenericAlgorithm  {
    private static final double mutationRate = 0.015;
    private static final int tournamentSize = 5;
    private static final boolean elitism = true;

    // Evolves a sum over one generation
    public static RoutesSum evolveSum(RoutesSum sum) {
        RoutesSum newSum = new RoutesSum(sum.getSize(), false);

        // Keep our best individual if elitism is enabled
        int elitismOffset = 0;
        if (elitism) {
            newSum.saveTour(0, sum.getFittest());
            elitismOffset = 1;
        }

        // Crossover sum
        // Loop over the new sum's size and create individuals from
        // Current sum
        for (int i = elitismOffset; i < newSum.getSize(); i++) {
            // Select parents
            Route parent1 = routeSelection(sum);
            Route parent2 = routeSelection(sum);
            // Crossover parents
            Route child = crossover(parent1, parent2);
            // Add child to new sum
            newSum.saveTour(i, child);
        }

        // Mutate the new population a bit to add some new genetic material
        for (int i = elitismOffset; i < newSum.getSize(); i++) {
            mutate(newSum.getRoute(i));
        }

        return newSum;
    }

    // Applies crossover to a set of parents and creates offspring
    public static Route crossover(Route parent1, Route parent2) {
        // Create new child route
        Route child = new Route();

        // Get start and end sub tour positions for parent1's route
        float startPos = (int) (Math.random() * parent1.getRouteSize());
        float endPos = (int) (Math.random() * parent1.getRouteSize());

        // Loop and add the sub tour from parent1 to our child
        for (int i = 0; i < child.getRouteSize(); i++) {
            // If our start position is less than the end position
            if (startPos < endPos && i > startPos && i < endPos) {
                child.setCity(i, parent1.getCity(i));
            } // If our start position is larger
            else if (startPos > endPos) {
                if (!(i < startPos && i > endPos)) {
                    child.setCity(i, parent1.getCity(i));
                }
            }
        }

        // Loop through parent2's city route
        for (int i = 0; i < parent2.getRouteSize(); i++) {
            // If child doesn't have the city add it
            if (!child.containsCity(parent2.getCity(i))) {
                // Loop to find a spare position in the child's route
                for (int ii = 0; ii < child.getRouteSize(); ii++) {
                    // Spare position found, add city
                    if (child.getCity(ii) == null) {
                        child.setCity(ii, parent2.getCity(i));
                        break;
                    }
                }
            }
        }
        return child;
    }

    // Mutate a route using swap mutation
    private static void mutate(Route route) {
        // Loop through tour cities
        for(int pos1=0; pos1 < route.getRouteSize(); pos1++){
            // Apply mutation rate
            if(Math.random() < mutationRate){
                // Get a second random position in the tour
                int pos2 = (int) (route.getRouteSize() * Math.random());

                // Get the cities at target position in route
                City city1 = route.getCity(pos1);
                City city2 = route.getCity(pos2);

                // Swap them around
                route.setCity(pos2, city1);
                route.setCity(pos1, city2);
            }
        }
    }

    // Selects candidate tour for crossover
    private static Route routeSelection(RoutesSum pop) {
        // Create a routes sum
        RoutesSum tournament = new RoutesSum(tournamentSize, false);
        // For each place in the path get a random candidate route and
        // add it
        for (int i = 0; i < tournamentSize; i++) {
            int randomId = (int) (Math.random() * pop.getSize());
            tournament.saveTour(i, pop.getRoute(randomId));
        }
        // Get the fittest route
        Route fittest = tournament.getFittest();
        return fittest;
    }
}

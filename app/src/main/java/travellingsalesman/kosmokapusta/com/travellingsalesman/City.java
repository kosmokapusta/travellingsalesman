package travellingsalesman.kosmokapusta.com.travellingsalesman;

public class City {
    float x;
    float y;
    String name;

    // Constructs a city at chosen x, y and name
    public City(float x, float y, String name){
        this.x = x;
        this.y = y;
        this.name = name;
    }

    // Gets city's x coordinate
    public float getX(){
        return this.x;
    }

    // Gets city's y coordinate
    public float getY(){
        return this.y;
    }

    // Gets city's name
    public String getName() { return this.name; }

    // Gets the distance to given city
    public double distanceTo(City city){
        float xDistance = Math.abs(getX() - city.getX());
        float yDistance = Math.abs(getY() - city.getY());
        double distance = Math.sqrt( (xDistance*xDistance) + (yDistance*yDistance) );

        return distance;
    }

    @Override
    public String toString(){
        return getX()+", "+getY();
    }
}

package travellingsalesman.kosmokapusta.com.travellingsalesman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by kosmokapusta on 09.01.15.
 */
public class DrawView extends View {
    private int pathColor = Color.argb(0xff, 0x99, 0x00, 0x00);
    private Path path;
    private Route route;
    private ArrayList<City> points;
    private Paint pathPaint;
    private Paint pointPaint;
    private Paint textPaint;
    private Context context;

    public DrawView (Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawView (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DrawView(Context context) {
        super(context);

        this.context = context;

        pathPaint = new Paint();
        pathPaint.setColor(pathColor);
        pathPaint.setStrokeWidth(10);
        pathPaint.setAntiAlias(true);
        pathPaint.setStrokeCap(Paint.Cap.ROUND);
        pathPaint.setStrokeJoin(Paint.Join.ROUND);
        pathPaint.setStyle(Paint.Style.STROKE);
        pathPaint.setShadowLayer(7, 0, 0, Color.RED);

        pointPaint = new Paint();
        pointPaint.setColor(pathColor);

        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(20);

        path = new Path();
        points = new ArrayList<>();
        route = new Route();
    }

    public void setPath (Route r) {
        Log.d("setPath", "route: " + r.getRouteSize());
        route = r;
    }

    public void setPoints(ArrayList<City> cities) {
        Log.d("setPoints", "cities: " + cities.size());
        points = cities;
    }


    @Override
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

        clearAll(canvas);

        if ( points.size() > 0 ) {
            for (int i = 0; i < points.size(); ++i) {
                canvas.drawCircle(points.get(i).getX(), points.get(i).getY(), 30, pointPaint);
            }

            if (route.getRouteSize() > 0) {
                path.moveTo(route.getCity(0).getX(), route.getCity(0).getY());

                for (int j = 0; j <= route.getRouteSize(); j++) {
                    if( j == route.getRouteSize()) {
                        path.lineTo(route.getCity(0).getX(), route.getCity(0).getY());
                    } else {
                        path.lineTo(route.getCity(j).getX(), route.getCity(j).getY());
                    }
                }

                canvas.drawPath(path, pathPaint);

                for (int i = 0; i < route.getRouteSize(); ++i) {
                    canvas.drawText(Integer.toString(i), route.getCity(i).getX(), route.getCity(i).getY(), textPaint);
                }
            }
        }
    }

    private void clearAll(Canvas canvas) {
        path = new Path();
        Paint clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawRect(0, 0, 0, 0, clearPaint);
    }

    public void reDraw() {
        this.invalidate();
    }
}

package travellingsalesman.kosmokapusta.com.travellingsalesman;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by kosmokapusta on 08.01.15.
 */
public class Route {
    private ArrayList route = new ArrayList<City>();
    // Cache
    private double fitness = 0;
    private int distance = 0;

    // Constructs a blank route
    public Route(){
        for (int i = 0; i < RouteManager.numberOfCities(); i++) {
            route.add(null);
        }
    }

    public Route(ArrayList tour){
        this.route = tour;
    }

    // Creates a random individual
    public void generateIndividual() {
        // Loop through all our destination cities and add them to our tour
        for (int cityIndex = 0; cityIndex < RouteManager.numberOfCities(); cityIndex++) {
            setCity(cityIndex, RouteManager.getCity(cityIndex));
        }
        // Randomly reorder the route
        Collections.shuffle(route);
    }

    // Gets a city from the route
    public City getCity(int position) {
        return (City)route.get(position);
    }

    // Sets a city in a certain position within a route
    public void setCity(int position, City city) {
        route.set(position, city);
        // Reset the fitness and distance
        fitness = 0;
        distance = 0;
    }

    // Gets the routes fitness
    public double getFitness() {
        if (fitness == 0) {
            fitness = 1/(double)getDistance();
        }
        return fitness;
    }

    // Gets the total distance of the tour
    public int getDistance(){
        if (distance == 0) {
            int tourDistance = 0;
            // Loop through our tour's cities
            for (int cityIndex=0; cityIndex < getRouteSize(); cityIndex++) {
                // Get city we're travelling from
                City fromCity = getCity(cityIndex);
                // City we're travelling to
                City destinationCity;
                // Check we're not on our tour's last city, if we are set our
                // tour's final destination city to our starting city
                if(cityIndex+1 < getRouteSize()){
                    destinationCity = getCity(cityIndex+1);
                }
                else{
                    destinationCity = getCity(0);
                }
                // Get the distance between the two cities
                tourDistance += fromCity.distanceTo(destinationCity);
            }
            distance = tourDistance;
        }
        return distance;
    }

    // Get number of cities on our tour
    public int getRouteSize() {
        return route.size();
    }

    // Check if the tour contains a city
    public boolean containsCity(City city){
        return route.contains(city);
    }

    public ArrayList<City> getRoute() {
        return route;
    }

    public void clear() {
        route.clear();
    }

    @Override
    public String toString() {
        String geneString = "|";
        for (int i = 0; i < getRouteSize(); i++) {
            geneString += getCity(i)+"|";
        }
        return geneString;
    }
}

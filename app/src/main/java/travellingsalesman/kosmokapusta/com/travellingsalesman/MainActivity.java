package travellingsalesman.kosmokapusta.com.travellingsalesman;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    private static String LOG = "MAIN";

    private DrawView drawView;
    private CheckBox isKasselIncluded;

    ArrayList<City> cities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawView = new DrawView(this);

        cities = new ArrayList<>();
        cities.add(new City(570, 160, "Berlin"));
        cities.add(new City(460, 640, "Munich"));
        cities.add(new City(220, 750, "Karlsruhe"));
        cities.add(new City(130, 250, "Oberhausen"));
        cities.add(new City(310, 180, "Hannover"));
        cities.add(new City(300, 300, "Kassel"));

        LinearLayout fl = new LinearLayout(this);
        fl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        fl.setOrientation(LinearLayout.VERTICAL);
        Button chooseWayBtn = new Button(this);
        chooseWayBtn.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        chooseWayBtn.setText("Choose the best way");

        chooseWayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doChooseWay();
            }
        });

        isKasselIncluded = new CheckBox(this);
        isKasselIncluded.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        isKasselIncluded.setText("Include Kassel");

        fl.addView(chooseWayBtn);
        fl.addView(isKasselIncluded);
        fl.addView(drawView);

        setContentView(fl);
        drawView.setPoints(cities);
        drawView.reDraw();
    }

    public void doChooseWay() {

        if(RouteManager.numberOfCities() > 0) {
            RouteManager.clear();
        }

        if (!isKasselIncluded.isChecked()) {
            // If Kassel is not checked made sublist without Kassel
            List<City> newList = cities.subList(0, cities.size() - 1);

            for (int i = 0; i < newList.size(); ++i) {
                RouteManager.addCity(newList.get(i));
            }
        } else {
            for (int i = 0; i < cities.size(); ++i) {
                RouteManager.addCity(cities.get(i));
            }
        }

        // Initialize
        RoutesSum sum = new RoutesSum(cities.size(), true);
        Log.d(LOG, "Initial distance: " + sum.getFittest().getDistance());

        // sum for 100 generations
        for (int i = 0; i < 100; i++) {
            sum = GenericAlgorithm.evolveSum(sum);
        }

        drawView.setPath(sum.getFittest());
        drawView.reDraw();

        Log.d(LOG, "Finished");
        Log.d(LOG, "Final distance: " + sum.getFittest().getDistance());
        Log.d(LOG, "Solution: " + sum.getFittest());
    }
}

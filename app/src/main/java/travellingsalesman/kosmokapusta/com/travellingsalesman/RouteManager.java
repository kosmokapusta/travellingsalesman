package travellingsalesman.kosmokapusta.com.travellingsalesman;

import java.util.ArrayList;

/**
 * Created by kosmokapusta on 08.01.15.
 */
public class RouteManager {
    // Holds our cities
    private static ArrayList destinationCities = new ArrayList<City>();

    // Adds a destination city
    public static void addCity(City city) {
        destinationCities.add(city);
    }

    // Get a city
    public static City getCity(int index){
        return (City)destinationCities.get(index);
    }

    // Get the number of destination cities
    public static int numberOfCities(){
        return destinationCities.size();
    }

    // Clear
    public static void clear(){ destinationCities.clear();  }
}

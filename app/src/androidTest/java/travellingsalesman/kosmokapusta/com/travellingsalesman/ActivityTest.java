package travellingsalesman.kosmokapusta.com.travellingsalesman;

import android.test.ActivityInstrumentationTestCase2;

public class ActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mainActivity;

    public ActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
    }

    public void testPreconditions() {
        assertNotNull("MainActivity is null", mainActivity);
    }
}